<?php
/**
 * MediaTest
 *
 * PHP version 5
 *
 * @category Class
 * @package  Swagger\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Wordpress v2 API
 *
 * Wordpress v2 API
 *
 * OpenAPI spec version: 0.1.0
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 2.4.25
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the model.
 */

namespace Swagger\Client;

/**
 * MediaTest Class Doc Comment
 *
 * @category    Class
 * @description Media
 * @package     Swagger\Client
 * @author      Swagger Codegen team
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class MediaTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test "Media"
     */
    public function testMedia()
    {
    }

    /**
     * Test attribute "date"
     */
    public function testPropertyDate()
    {
    }

    /**
     * Test attribute "date_gmt"
     */
    public function testPropertyDateGmt()
    {
    }

    /**
     * Test attribute "guid"
     */
    public function testPropertyGuid()
    {
    }

    /**
     * Test attribute "id"
     */
    public function testPropertyId()
    {
    }

    /**
     * Test attribute "modified"
     */
    public function testPropertyModified()
    {
    }

    /**
     * Test attribute "modified_gmt"
     */
    public function testPropertyModifiedGmt()
    {
    }

    /**
     * Test attribute "password"
     */
    public function testPropertyPassword()
    {
    }

    /**
     * Test attribute "slug"
     */
    public function testPropertySlug()
    {
    }

    /**
     * Test attribute "status"
     */
    public function testPropertyStatus()
    {
    }

    /**
     * Test attribute "type"
     */
    public function testPropertyType()
    {
    }

    /**
     * Test attribute "title"
     */
    public function testPropertyTitle()
    {
    }

    /**
     * Test attribute "author"
     */
    public function testPropertyAuthor()
    {
    }

    /**
     * Test attribute "comment_status"
     */
    public function testPropertyCommentStatus()
    {
    }

    /**
     * Test attribute "ping_status"
     */
    public function testPropertyPingStatus()
    {
    }

    /**
     * Test attribute "alt_text"
     */
    public function testPropertyAltText()
    {
    }

    /**
     * Test attribute "caption"
     */
    public function testPropertyCaption()
    {
    }

    /**
     * Test attribute "description"
     */
    public function testPropertyDescription()
    {
    }

    /**
     * Test attribute "media_type"
     */
    public function testPropertyMediaType()
    {
    }

    /**
     * Test attribute "mime_type"
     */
    public function testPropertyMimeType()
    {
    }

    /**
     * Test attribute "media_details"
     */
    public function testPropertyMediaDetails()
    {
    }

    /**
     * Test attribute "post"
     */
    public function testPropertyPost()
    {
    }

    /**
     * Test attribute "source_url"
     */
    public function testPropertySourceUrl()
    {
    }
}
