<?php
/**
 * CommonPostPageTest
 *
 * PHP version 5
 *
 * @category Class
 * @package  Swagger\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Wordpress v2 API
 *
 * Wordpress v2 API
 *
 * OpenAPI spec version: 0.1.0
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 2.4.25
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the model.
 */

namespace Swagger\Client;

/**
 * CommonPostPageTest Class Doc Comment
 *
 * @category    Class
 * @description CommonPostPage
 * @package     Swagger\Client
 * @author      Swagger Codegen team
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class CommonPostPageTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test "CommonPostPage"
     */
    public function testCommonPostPage()
    {
    }

    /**
     * Test attribute "date"
     */
    public function testPropertyDate()
    {
    }

    /**
     * Test attribute "date_gmt"
     */
    public function testPropertyDateGmt()
    {
    }

    /**
     * Test attribute "guid"
     */
    public function testPropertyGuid()
    {
    }

    /**
     * Test attribute "id"
     */
    public function testPropertyId()
    {
    }

    /**
     * Test attribute "link"
     */
    public function testPropertyLink()
    {
    }

    /**
     * Test attribute "modified"
     */
    public function testPropertyModified()
    {
    }

    /**
     * Test attribute "modified_gmt"
     */
    public function testPropertyModifiedGmt()
    {
    }

    /**
     * Test attribute "password"
     */
    public function testPropertyPassword()
    {
    }

    /**
     * Test attribute "slug"
     */
    public function testPropertySlug()
    {
    }

    /**
     * Test attribute "status"
     */
    public function testPropertyStatus()
    {
    }

    /**
     * Test attribute "type"
     */
    public function testPropertyType()
    {
    }

    /**
     * Test attribute "title"
     */
    public function testPropertyTitle()
    {
    }

    /**
     * Test attribute "content"
     */
    public function testPropertyContent()
    {
    }

    /**
     * Test attribute "author"
     */
    public function testPropertyAuthor()
    {
    }

    /**
     * Test attribute "excerpt"
     */
    public function testPropertyExcerpt()
    {
    }

    /**
     * Test attribute "featured_media"
     */
    public function testPropertyFeaturedMedia()
    {
    }

    /**
     * Test attribute "comment_status"
     */
    public function testPropertyCommentStatus()
    {
    }

    /**
     * Test attribute "ping_status"
     */
    public function testPropertyPingStatus()
    {
    }
}
