# Swagger\Client\DefaultApi

All URIs are relative to *http://localhost/wp-json/wp/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**categoriesGet**](DefaultApi.md#categoriesGet) | **GET** /categories | List categories
[**categoriesIdDelete**](DefaultApi.md#categoriesIdDelete) | **DELETE** /categories/{id} | Delete Single Category
[**categoriesIdGet**](DefaultApi.md#categoriesIdGet) | **GET** /categories/{id} | Get Single Category
[**categoriesIdPost**](DefaultApi.md#categoriesIdPost) | **POST** /categories/{id} | Update Single Category
[**categoriesPost**](DefaultApi.md#categoriesPost) | **POST** /categories | Create Category
[**commentsGet**](DefaultApi.md#commentsGet) | **GET** /comments | List Comments
[**commentsIdDelete**](DefaultApi.md#commentsIdDelete) | **DELETE** /comments/{id} | Delete Single Comment
[**commentsIdGet**](DefaultApi.md#commentsIdGet) | **GET** /comments/{id} | Get Single Comment
[**commentsIdPost**](DefaultApi.md#commentsIdPost) | **POST** /comments/{id} | Update Single Comment
[**commentsPost**](DefaultApi.md#commentsPost) | **POST** /comments | Create Comment
[**mediaGet**](DefaultApi.md#mediaGet) | **GET** /media | List Media
[**mediaIdDelete**](DefaultApi.md#mediaIdDelete) | **DELETE** /media/{id} | Delete Single Media
[**mediaIdGet**](DefaultApi.md#mediaIdGet) | **GET** /media/{id} | Get Single Media
[**mediaIdPost**](DefaultApi.md#mediaIdPost) | **POST** /media/{id} | Update Single Media
[**mediaPost**](DefaultApi.md#mediaPost) | **POST** /media | Create Media
[**pagesGet**](DefaultApi.md#pagesGet) | **GET** /pages | List Pages
[**pagesIdDelete**](DefaultApi.md#pagesIdDelete) | **DELETE** /pages/{id} | Delete Single Page
[**pagesIdGet**](DefaultApi.md#pagesIdGet) | **GET** /pages/{id} | Get Single Page
[**pagesIdPost**](DefaultApi.md#pagesIdPost) | **POST** /pages/{id} | Update Single Page
[**pagesPost**](DefaultApi.md#pagesPost) | **POST** /pages | Create Page
[**postsGet**](DefaultApi.md#postsGet) | **GET** /posts | List Posts
[**postsIdDelete**](DefaultApi.md#postsIdDelete) | **DELETE** /posts/{id} | Delete Single Post
[**postsIdGet**](DefaultApi.md#postsIdGet) | **GET** /posts/{id} | Get Single Post
[**postsIdPost**](DefaultApi.md#postsIdPost) | **POST** /posts/{id} | Update Single Post
[**postsIdRevisionsGet**](DefaultApi.md#postsIdRevisionsGet) | **GET** /posts/{id}/revisions | Get post revisions
[**postsIdRevisionsRevisionidDelete**](DefaultApi.md#postsIdRevisionsRevisionidDelete) | **DELETE** /posts/{id}/revisions/{revisionid} | Delete single post revisions
[**postsIdRevisionsRevisionidGet**](DefaultApi.md#postsIdRevisionsRevisionidGet) | **GET** /posts/{id}/revisions/{revisionid} | Get single post revisions
[**postsPost**](DefaultApi.md#postsPost) | **POST** /posts | Create Post
[**statusesGet**](DefaultApi.md#statusesGet) | **GET** /statuses | List Status
[**statusesIdGet**](DefaultApi.md#statusesIdGet) | **GET** /statuses/{id} | Get Single Status
[**tagsGet**](DefaultApi.md#tagsGet) | **GET** /tags | List Tags
[**tagsIdDelete**](DefaultApi.md#tagsIdDelete) | **DELETE** /tags/{id} | Delete Single Tag
[**tagsIdGet**](DefaultApi.md#tagsIdGet) | **GET** /tags/{id} | Get Single Tag
[**tagsIdPost**](DefaultApi.md#tagsIdPost) | **POST** /tags/{id} | Update Single Tag
[**tagsPost**](DefaultApi.md#tagsPost) | **POST** /tags | Create Tag
[**taxonomiesGet**](DefaultApi.md#taxonomiesGet) | **GET** /taxonomies | List Taxonomy
[**taxonomiesIdGet**](DefaultApi.md#taxonomiesIdGet) | **GET** /taxonomies/{id} | Get Single Taxonomy
[**typesGet**](DefaultApi.md#typesGet) | **GET** /types | List Type
[**typesIdGet**](DefaultApi.md#typesIdGet) | **GET** /types/{id} | Get Single Type
[**usersGet**](DefaultApi.md#usersGet) | **GET** /users | List Tags
[**usersIdDelete**](DefaultApi.md#usersIdDelete) | **DELETE** /users/{id} | Delete Single User
[**usersIdGet**](DefaultApi.md#usersIdGet) | **GET** /users/{id} | Get Single User
[**usersIdPost**](DefaultApi.md#usersIdPost) | **POST** /users/{id} | Update Single User
[**usersPost**](DefaultApi.md#usersPost) | **POST** /users | Create User


# **categoriesGet**
> \Swagger\Client\Model\Category[] categoriesGet($context, $page, $per_page, $search, $hide_empty, $exclude, $include, $order, $orderby, $parent, $post, $slug)

List categories

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\DefaultApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$context = "context_example"; // string | Scope under which the request is made; determines fields present in response.
$page = 56; // int | Current page of the collection. Default 1
$per_page = 56; // int | Maximum number of items to be returned in result set. Default 10
$search = "search_example"; // string | Limit results to those matching a string.
$hide_empty = true; // bool | Whether to hide resources not assigned to any posts.
$exclude = "exclude_example"; // string | Ensure result set excludes specific ids.
$include = "include_example"; // string | Ensure result set includes specific ids.
$order = "order_example"; // string | Order sort attribute ascending or descending. Default desc
$orderby = "orderby_example"; // string | Sort collection by object attribute. Default date
$parent = 56; // int | The id for the parent of the object.
$post = "post_example"; // string | The id for the associated post of the resource.
$slug = "slug_example"; // string | Limit result set to posts with a specific slug.

try {
    $result = $apiInstance->categoriesGet($context, $page, $per_page, $search, $hide_empty, $exclude, $include, $order, $orderby, $parent, $post, $slug);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DefaultApi->categoriesGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **context** | **string**| Scope under which the request is made; determines fields present in response. | [optional]
 **page** | **int**| Current page of the collection. Default 1 | [optional]
 **per_page** | **int**| Maximum number of items to be returned in result set. Default 10 | [optional]
 **search** | **string**| Limit results to those matching a string. | [optional]
 **hide_empty** | **bool**| Whether to hide resources not assigned to any posts. | [optional]
 **exclude** | **string**| Ensure result set excludes specific ids. | [optional]
 **include** | **string**| Ensure result set includes specific ids. | [optional]
 **order** | **string**| Order sort attribute ascending or descending. Default desc | [optional]
 **orderby** | **string**| Sort collection by object attribute. Default date | [optional]
 **parent** | **int**| The id for the parent of the object. | [optional]
 **post** | **string**| The id for the associated post of the resource. | [optional]
 **slug** | **string**| Limit result set to posts with a specific slug. | [optional]

### Return type

[**\Swagger\Client\Model\Category[]**](../Model/Category.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **categoriesIdDelete**
> categoriesIdDelete($id, $force)

Delete Single Category

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\DefaultApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = "id_example"; // string | Id of object
$force = true; // bool | Whether to bypass trash and force deletion.

try {
    $apiInstance->categoriesIdDelete($id, $force);
} catch (Exception $e) {
    echo 'Exception when calling DefaultApi->categoriesIdDelete: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Id of object |
 **force** | **bool**| Whether to bypass trash and force deletion. | [optional]

### Return type

void (empty response body)

### Authorization

[oauth](../../README.md#oauth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **categoriesIdGet**
> \Swagger\Client\Model\Category categoriesIdGet($id, $context)

Get Single Category

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\DefaultApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = "id_example"; // string | Id of object
$context = "context_example"; // string | Scope under which the request is made; determines fields present in response.

try {
    $result = $apiInstance->categoriesIdGet($id, $context);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DefaultApi->categoriesIdGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Id of object |
 **context** | **string**| Scope under which the request is made; determines fields present in response. | [optional]

### Return type

[**\Swagger\Client\Model\Category**](../Model/Category.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **categoriesIdPost**
> \Swagger\Client\Model\Category categoriesIdPost($id, $name, $description, $slug, $parent)

Update Single Category

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\DefaultApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = "id_example"; // string | Id of object
$name = "name_example"; // string | HTML title for the resource.
$description = "description_example"; // string | The description for the resource
$slug = "slug_example"; // string | Limit result set to posts with a specific slug.
$parent = 56; // int | The id for the parent of the object.

try {
    $result = $apiInstance->categoriesIdPost($id, $name, $description, $slug, $parent);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DefaultApi->categoriesIdPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Id of object |
 **name** | **string**| HTML title for the resource. |
 **description** | **string**| The description for the resource | [optional]
 **slug** | **string**| Limit result set to posts with a specific slug. | [optional]
 **parent** | **int**| The id for the parent of the object. | [optional]

### Return type

[**\Swagger\Client\Model\Category**](../Model/Category.md)

### Authorization

[oauth](../../README.md#oauth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **categoriesPost**
> \Swagger\Client\Model\Category categoriesPost($name, $description, $slug, $parent)

Create Category

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\DefaultApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$name = "name_example"; // string | HTML title for the resource.
$description = "description_example"; // string | The description for the resource
$slug = "slug_example"; // string | Limit result set to posts with a specific slug.
$parent = 56; // int | The id for the parent of the object.

try {
    $result = $apiInstance->categoriesPost($name, $description, $slug, $parent);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DefaultApi->categoriesPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **string**| HTML title for the resource. |
 **description** | **string**| The description for the resource | [optional]
 **slug** | **string**| Limit result set to posts with a specific slug. | [optional]
 **parent** | **int**| The id for the parent of the object. | [optional]

### Return type

[**\Swagger\Client\Model\Category**](../Model/Category.md)

### Authorization

[oauth](../../README.md#oauth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **commentsGet**
> \Swagger\Client\Model\Comment[] commentsGet($context, $page, $per_page, $search, $after, $author, $author_exclude, $author_email, $before, $exclude, $include, $karma, $offset, $order, $orderby, $parent, $parent_exclude, $post, $status, $type)

List Comments

Scope under which the request is made; determines fields present in response.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\DefaultApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$context = "context_example"; // string | Scope under which the request is made; determines fields present in response.
$page = 56; // int | Current page of the collection. Default 1
$per_page = 56; // int | Maximum number of items to be returned in result set. Default 10
$search = "search_example"; // string | Limit results to those matching a string.
$after = "after_example"; // string | Limit response to resources published after a given ISO8601 compliant date.
$author = "author_example"; // string | Limit result set to posts assigned to specific authors.
$author_exclude = "author_exclude_example"; // string | Ensure result set excludes posts assigned to specific authors.
$author_email = "author_email_example"; // string | Limit result set to that from a specific author email. Requires authorization.
$before = "before_example"; // string | Limit response to resources published before a given ISO8601 compliant date.
$exclude = "exclude_example"; // string | Ensure result set excludes specific ids.
$include = "include_example"; // string | Ensure result set includes specific ids.
$karma = "karma_example"; // string | Limit result set to that of a particular comment karma. Requires authorization
$offset = "offset_example"; // string | Offset the result set by a specific number of items.
$order = "order_example"; // string | Order sort attribute ascending or descending. Default desc
$orderby = "orderby_example"; // string | Sort collection by object attribute. Default date
$parent = 56; // int | The id for the parent of the object.
$parent_exclude = "parent_exclude_example"; // string | Ensure result set excludes specific ids.
$post = "post_example"; // string | The id for the associated post of the resource.
$status = "status_example"; // string | Limit result set to posts assigned a specific status.Default publish
$type = "type_example"; // string | Limit result set to comments assigned a specific type. Requires authorization. Default comment

try {
    $result = $apiInstance->commentsGet($context, $page, $per_page, $search, $after, $author, $author_exclude, $author_email, $before, $exclude, $include, $karma, $offset, $order, $orderby, $parent, $parent_exclude, $post, $status, $type);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DefaultApi->commentsGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **context** | **string**| Scope under which the request is made; determines fields present in response. | [optional]
 **page** | **int**| Current page of the collection. Default 1 | [optional]
 **per_page** | **int**| Maximum number of items to be returned in result set. Default 10 | [optional]
 **search** | **string**| Limit results to those matching a string. | [optional]
 **after** | **string**| Limit response to resources published after a given ISO8601 compliant date. | [optional]
 **author** | **string**| Limit result set to posts assigned to specific authors. | [optional]
 **author_exclude** | **string**| Ensure result set excludes posts assigned to specific authors. | [optional]
 **author_email** | **string**| Limit result set to that from a specific author email. Requires authorization. | [optional]
 **before** | **string**| Limit response to resources published before a given ISO8601 compliant date. | [optional]
 **exclude** | **string**| Ensure result set excludes specific ids. | [optional]
 **include** | **string**| Ensure result set includes specific ids. | [optional]
 **karma** | **string**| Limit result set to that of a particular comment karma. Requires authorization | [optional]
 **offset** | **string**| Offset the result set by a specific number of items. | [optional]
 **order** | **string**| Order sort attribute ascending or descending. Default desc | [optional]
 **orderby** | **string**| Sort collection by object attribute. Default date | [optional]
 **parent** | **int**| The id for the parent of the object. | [optional]
 **parent_exclude** | **string**| Ensure result set excludes specific ids. | [optional]
 **post** | **string**| The id for the associated post of the resource. | [optional]
 **status** | **string**| Limit result set to posts assigned a specific status.Default publish | [optional]
 **type** | **string**| Limit result set to comments assigned a specific type. Requires authorization. Default comment | [optional]

### Return type

[**\Swagger\Client\Model\Comment[]**](../Model/Comment.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **commentsIdDelete**
> commentsIdDelete($id, $force)

Delete Single Comment

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\DefaultApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = "id_example"; // string | Id of object
$force = true; // bool | Whether to bypass trash and force deletion.

try {
    $apiInstance->commentsIdDelete($id, $force);
} catch (Exception $e) {
    echo 'Exception when calling DefaultApi->commentsIdDelete: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Id of object |
 **force** | **bool**| Whether to bypass trash and force deletion. | [optional]

### Return type

void (empty response body)

### Authorization

[oauth](../../README.md#oauth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **commentsIdGet**
> \Swagger\Client\Model\Comment commentsIdGet($id, $context)

Get Single Comment

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\DefaultApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = "id_example"; // string | Id of object
$context = "context_example"; // string | Scope under which the request is made; determines fields present in response.

try {
    $result = $apiInstance->commentsIdGet($id, $context);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DefaultApi->commentsIdGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Id of object |
 **context** | **string**| Scope under which the request is made; determines fields present in response. | [optional]

### Return type

[**\Swagger\Client\Model\Comment**](../Model/Comment.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **commentsIdPost**
> \Swagger\Client\Model\Comment commentsIdPost($id, $date, $date_gmt, $password, $slug, $status, $title, $author, $comment_status, $ping_status, $alt_text, $caption, $description, $post)

Update Single Comment

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\DefaultApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = "id_example"; // string | Id of object
$date = new \DateTime("2013-10-20T19:20:30+01:00"); // \DateTime | The date the object was published, in the site's timezone.
$date_gmt = new \DateTime("2013-10-20T19:20:30+01:00"); // \DateTime | The date the object was published, as GMT.
$password = "password_example"; // string | The A password to protect access to the post.
$slug = "slug_example"; // string | Limit result set to posts with a specific slug.
$status = "status_example"; // string | Limit result set to posts assigned a specific status.Default publish
$title = "title_example"; // string | The title for the object.
$author = "author_example"; // string | Limit result set to posts assigned to specific authors.
$comment_status = "comment_status_example"; // string | Whether or not comments are open on the object
$ping_status = "ping_status_example"; // string | Whether or not the object can be pinged.
$alt_text = "alt_text_example"; // string | Alternative text to display when resource is not displayed.
$caption = "caption_example"; // string | The caption for the resource.
$description = "description_example"; // string | The description for the resource
$post = "post_example"; // string | The id for the associated post of the resource.

try {
    $result = $apiInstance->commentsIdPost($id, $date, $date_gmt, $password, $slug, $status, $title, $author, $comment_status, $ping_status, $alt_text, $caption, $description, $post);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DefaultApi->commentsIdPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Id of object |
 **date** | **\DateTime**| The date the object was published, in the site&#39;s timezone. | [optional]
 **date_gmt** | **\DateTime**| The date the object was published, as GMT. | [optional]
 **password** | **string**| The A password to protect access to the post. | [optional]
 **slug** | **string**| Limit result set to posts with a specific slug. | [optional]
 **status** | **string**| Limit result set to posts assigned a specific status.Default publish | [optional]
 **title** | **string**| The title for the object. | [optional]
 **author** | **string**| Limit result set to posts assigned to specific authors. | [optional]
 **comment_status** | **string**| Whether or not comments are open on the object | [optional]
 **ping_status** | **string**| Whether or not the object can be pinged. | [optional]
 **alt_text** | **string**| Alternative text to display when resource is not displayed. | [optional]
 **caption** | **string**| The caption for the resource. | [optional]
 **description** | **string**| The description for the resource | [optional]
 **post** | **string**| The id for the associated post of the resource. | [optional]

### Return type

[**\Swagger\Client\Model\Comment**](../Model/Comment.md)

### Authorization

[oauth](../../README.md#oauth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **commentsPost**
> \Swagger\Client\Model\Comment commentsPost($date, $date_gmt, $password, $slug, $status, $title, $author, $comment_status, $ping_status, $alt_text, $caption, $description, $post)

Create Comment

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\DefaultApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$date = new \DateTime("2013-10-20T19:20:30+01:00"); // \DateTime | The date the object was published, in the site's timezone.
$date_gmt = new \DateTime("2013-10-20T19:20:30+01:00"); // \DateTime | The date the object was published, as GMT.
$password = "password_example"; // string | The A password to protect access to the post.
$slug = "slug_example"; // string | Limit result set to posts with a specific slug.
$status = "status_example"; // string | Limit result set to posts assigned a specific status.Default publish
$title = "title_example"; // string | The title for the object.
$author = "author_example"; // string | Limit result set to posts assigned to specific authors.
$comment_status = "comment_status_example"; // string | Whether or not comments are open on the object
$ping_status = "ping_status_example"; // string | Whether or not the object can be pinged.
$alt_text = "alt_text_example"; // string | Alternative text to display when resource is not displayed.
$caption = "caption_example"; // string | The caption for the resource.
$description = "description_example"; // string | The description for the resource
$post = "post_example"; // string | The id for the associated post of the resource.

try {
    $result = $apiInstance->commentsPost($date, $date_gmt, $password, $slug, $status, $title, $author, $comment_status, $ping_status, $alt_text, $caption, $description, $post);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DefaultApi->commentsPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **date** | **\DateTime**| The date the object was published, in the site&#39;s timezone. | [optional]
 **date_gmt** | **\DateTime**| The date the object was published, as GMT. | [optional]
 **password** | **string**| The A password to protect access to the post. | [optional]
 **slug** | **string**| Limit result set to posts with a specific slug. | [optional]
 **status** | **string**| Limit result set to posts assigned a specific status.Default publish | [optional]
 **title** | **string**| The title for the object. | [optional]
 **author** | **string**| Limit result set to posts assigned to specific authors. | [optional]
 **comment_status** | **string**| Whether or not comments are open on the object | [optional]
 **ping_status** | **string**| Whether or not the object can be pinged. | [optional]
 **alt_text** | **string**| Alternative text to display when resource is not displayed. | [optional]
 **caption** | **string**| The caption for the resource. | [optional]
 **description** | **string**| The description for the resource | [optional]
 **post** | **string**| The id for the associated post of the resource. | [optional]

### Return type

[**\Swagger\Client\Model\Comment**](../Model/Comment.md)

### Authorization

[oauth](../../README.md#oauth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **mediaGet**
> \Swagger\Client\Model\Media[] mediaGet($context, $page, $per_page, $search, $after, $author, $author_exclude, $before, $exclude, $include, $offset, $order, $orderby, $parent, $parent_exclude, $slug, $status, $filter, $media_type, $mime_type)

List Media

Scope under which the request is made; determines fields present in response.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\DefaultApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$context = "context_example"; // string | Scope under which the request is made; determines fields present in response.
$page = 56; // int | Current page of the collection. Default 1
$per_page = 56; // int | Maximum number of items to be returned in result set. Default 10
$search = "search_example"; // string | Limit results to those matching a string.
$after = "after_example"; // string | Limit response to resources published after a given ISO8601 compliant date.
$author = "author_example"; // string | Limit result set to posts assigned to specific authors.
$author_exclude = "author_exclude_example"; // string | Ensure result set excludes posts assigned to specific authors.
$before = "before_example"; // string | Limit response to resources published before a given ISO8601 compliant date.
$exclude = "exclude_example"; // string | Ensure result set excludes specific ids.
$include = "include_example"; // string | Ensure result set includes specific ids.
$offset = "offset_example"; // string | Offset the result set by a specific number of items.
$order = "order_example"; // string | Order sort attribute ascending or descending. Default desc
$orderby = "orderby_example"; // string | Sort collection by object attribute. Default date
$parent = 56; // int | The id for the parent of the object.
$parent_exclude = "parent_exclude_example"; // string | Ensure result set excludes specific ids.
$slug = "slug_example"; // string | Limit result set to posts with a specific slug.
$status = "status_example"; // string | Limit result set to posts assigned a specific status.Default publish
$filter = "filter_example"; // string | Use WP Query arguments to modify the response; private query vars require appropriate authorization.
$media_type = "media_type_example"; // string | Type of resource.
$mime_type = "mime_type_example"; // string | Alternative text to display when resource is not displayed.

try {
    $result = $apiInstance->mediaGet($context, $page, $per_page, $search, $after, $author, $author_exclude, $before, $exclude, $include, $offset, $order, $orderby, $parent, $parent_exclude, $slug, $status, $filter, $media_type, $mime_type);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DefaultApi->mediaGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **context** | **string**| Scope under which the request is made; determines fields present in response. | [optional]
 **page** | **int**| Current page of the collection. Default 1 | [optional]
 **per_page** | **int**| Maximum number of items to be returned in result set. Default 10 | [optional]
 **search** | **string**| Limit results to those matching a string. | [optional]
 **after** | **string**| Limit response to resources published after a given ISO8601 compliant date. | [optional]
 **author** | **string**| Limit result set to posts assigned to specific authors. | [optional]
 **author_exclude** | **string**| Ensure result set excludes posts assigned to specific authors. | [optional]
 **before** | **string**| Limit response to resources published before a given ISO8601 compliant date. | [optional]
 **exclude** | **string**| Ensure result set excludes specific ids. | [optional]
 **include** | **string**| Ensure result set includes specific ids. | [optional]
 **offset** | **string**| Offset the result set by a specific number of items. | [optional]
 **order** | **string**| Order sort attribute ascending or descending. Default desc | [optional]
 **orderby** | **string**| Sort collection by object attribute. Default date | [optional]
 **parent** | **int**| The id for the parent of the object. | [optional]
 **parent_exclude** | **string**| Ensure result set excludes specific ids. | [optional]
 **slug** | **string**| Limit result set to posts with a specific slug. | [optional]
 **status** | **string**| Limit result set to posts assigned a specific status.Default publish | [optional]
 **filter** | **string**| Use WP Query arguments to modify the response; private query vars require appropriate authorization. | [optional]
 **media_type** | **string**| Type of resource. | [optional]
 **mime_type** | **string**| Alternative text to display when resource is not displayed. | [optional]

### Return type

[**\Swagger\Client\Model\Media[]**](../Model/Media.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **mediaIdDelete**
> mediaIdDelete($id, $force)

Delete Single Media

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\DefaultApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = "id_example"; // string | Id of object
$force = true; // bool | Whether to bypass trash and force deletion.

try {
    $apiInstance->mediaIdDelete($id, $force);
} catch (Exception $e) {
    echo 'Exception when calling DefaultApi->mediaIdDelete: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Id of object |
 **force** | **bool**| Whether to bypass trash and force deletion. | [optional]

### Return type

void (empty response body)

### Authorization

[oauth](../../README.md#oauth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **mediaIdGet**
> \Swagger\Client\Model\Media mediaIdGet($id, $context)

Get Single Media

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\DefaultApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = "id_example"; // string | Id of object
$context = "context_example"; // string | Scope under which the request is made; determines fields present in response.

try {
    $result = $apiInstance->mediaIdGet($id, $context);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DefaultApi->mediaIdGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Id of object |
 **context** | **string**| Scope under which the request is made; determines fields present in response. | [optional]

### Return type

[**\Swagger\Client\Model\Media**](../Model/Media.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **mediaIdPost**
> \Swagger\Client\Model\Media mediaIdPost($id, $date, $date_gmt, $password, $slug, $status, $title, $author, $comment_status, $ping_status, $alt_text, $caption, $description, $post)

Update Single Media

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\DefaultApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = "id_example"; // string | Id of object
$date = new \DateTime("2013-10-20T19:20:30+01:00"); // \DateTime | The date the object was published, in the site's timezone.
$date_gmt = new \DateTime("2013-10-20T19:20:30+01:00"); // \DateTime | The date the object was published, as GMT.
$password = "password_example"; // string | The A password to protect access to the post.
$slug = "slug_example"; // string | Limit result set to posts with a specific slug.
$status = "status_example"; // string | Limit result set to posts assigned a specific status.Default publish
$title = "title_example"; // string | The title for the object.
$author = "author_example"; // string | Limit result set to posts assigned to specific authors.
$comment_status = "comment_status_example"; // string | Whether or not comments are open on the object
$ping_status = "ping_status_example"; // string | Whether or not the object can be pinged.
$alt_text = "alt_text_example"; // string | Alternative text to display when resource is not displayed.
$caption = "caption_example"; // string | The caption for the resource.
$description = "description_example"; // string | The description for the resource
$post = "post_example"; // string | The id for the associated post of the resource.

try {
    $result = $apiInstance->mediaIdPost($id, $date, $date_gmt, $password, $slug, $status, $title, $author, $comment_status, $ping_status, $alt_text, $caption, $description, $post);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DefaultApi->mediaIdPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Id of object |
 **date** | **\DateTime**| The date the object was published, in the site&#39;s timezone. | [optional]
 **date_gmt** | **\DateTime**| The date the object was published, as GMT. | [optional]
 **password** | **string**| The A password to protect access to the post. | [optional]
 **slug** | **string**| Limit result set to posts with a specific slug. | [optional]
 **status** | **string**| Limit result set to posts assigned a specific status.Default publish | [optional]
 **title** | **string**| The title for the object. | [optional]
 **author** | **string**| Limit result set to posts assigned to specific authors. | [optional]
 **comment_status** | **string**| Whether or not comments are open on the object | [optional]
 **ping_status** | **string**| Whether or not the object can be pinged. | [optional]
 **alt_text** | **string**| Alternative text to display when resource is not displayed. | [optional]
 **caption** | **string**| The caption for the resource. | [optional]
 **description** | **string**| The description for the resource | [optional]
 **post** | **string**| The id for the associated post of the resource. | [optional]

### Return type

[**\Swagger\Client\Model\Media**](../Model/Media.md)

### Authorization

[oauth](../../README.md#oauth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **mediaPost**
> \Swagger\Client\Model\Media mediaPost($date, $date_gmt, $password, $slug, $status, $title, $author, $comment_status, $ping_status, $alt_text, $caption, $description, $post)

Create Media

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\DefaultApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$date = new \DateTime("2013-10-20T19:20:30+01:00"); // \DateTime | The date the object was published, in the site's timezone.
$date_gmt = new \DateTime("2013-10-20T19:20:30+01:00"); // \DateTime | The date the object was published, as GMT.
$password = "password_example"; // string | The A password to protect access to the post.
$slug = "slug_example"; // string | Limit result set to posts with a specific slug.
$status = "status_example"; // string | Limit result set to posts assigned a specific status.Default publish
$title = "title_example"; // string | The title for the object.
$author = "author_example"; // string | Limit result set to posts assigned to specific authors.
$comment_status = "comment_status_example"; // string | Whether or not comments are open on the object
$ping_status = "ping_status_example"; // string | Whether or not the object can be pinged.
$alt_text = "alt_text_example"; // string | Alternative text to display when resource is not displayed.
$caption = "caption_example"; // string | The caption for the resource.
$description = "description_example"; // string | The description for the resource
$post = "post_example"; // string | The id for the associated post of the resource.

try {
    $result = $apiInstance->mediaPost($date, $date_gmt, $password, $slug, $status, $title, $author, $comment_status, $ping_status, $alt_text, $caption, $description, $post);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DefaultApi->mediaPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **date** | **\DateTime**| The date the object was published, in the site&#39;s timezone. | [optional]
 **date_gmt** | **\DateTime**| The date the object was published, as GMT. | [optional]
 **password** | **string**| The A password to protect access to the post. | [optional]
 **slug** | **string**| Limit result set to posts with a specific slug. | [optional]
 **status** | **string**| Limit result set to posts assigned a specific status.Default publish | [optional]
 **title** | **string**| The title for the object. | [optional]
 **author** | **string**| Limit result set to posts assigned to specific authors. | [optional]
 **comment_status** | **string**| Whether or not comments are open on the object | [optional]
 **ping_status** | **string**| Whether or not the object can be pinged. | [optional]
 **alt_text** | **string**| Alternative text to display when resource is not displayed. | [optional]
 **caption** | **string**| The caption for the resource. | [optional]
 **description** | **string**| The description for the resource | [optional]
 **post** | **string**| The id for the associated post of the resource. | [optional]

### Return type

[**\Swagger\Client\Model\Media**](../Model/Media.md)

### Authorization

[oauth](../../README.md#oauth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **pagesGet**
> \Swagger\Client\Model\Page[] pagesGet($context, $page, $per_page, $search, $after, $author, $author_exclude, $before, $exclude, $include, $menu_order, $offset, $order, $orderby, $parent, $parent_exclude, $slug, $status, $filter)

List Pages

Scope under which the request is made; determines fields present in response.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\DefaultApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$context = "context_example"; // string | Scope under which the request is made; determines fields present in response.
$page = 56; // int | Current page of the collection. Default 1
$per_page = 56; // int | Maximum number of items to be returned in result set. Default 10
$search = "search_example"; // string | Limit results to those matching a string.
$after = "after_example"; // string | Limit response to resources published after a given ISO8601 compliant date.
$author = "author_example"; // string | Limit result set to posts assigned to specific authors.
$author_exclude = "author_exclude_example"; // string | Ensure result set excludes posts assigned to specific authors.
$before = "before_example"; // string | Limit response to resources published before a given ISO8601 compliant date.
$exclude = "exclude_example"; // string | Ensure result set excludes specific ids.
$include = "include_example"; // string | Ensure result set includes specific ids.
$menu_order = 56; // int | The order of the object in relation to other object of its type.
$offset = "offset_example"; // string | Offset the result set by a specific number of items.
$order = "order_example"; // string | Order sort attribute ascending or descending. Default desc
$orderby = "orderby_example"; // string | Sort collection by object attribute. Default date
$parent = 56; // int | The id for the parent of the object.
$parent_exclude = "parent_exclude_example"; // string | Ensure result set excludes specific ids.
$slug = "slug_example"; // string | Limit result set to posts with a specific slug.
$status = "status_example"; // string | Limit result set to posts assigned a specific status.Default publish
$filter = "filter_example"; // string | Use WP Query arguments to modify the response; private query vars require appropriate authorization.

try {
    $result = $apiInstance->pagesGet($context, $page, $per_page, $search, $after, $author, $author_exclude, $before, $exclude, $include, $menu_order, $offset, $order, $orderby, $parent, $parent_exclude, $slug, $status, $filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DefaultApi->pagesGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **context** | **string**| Scope under which the request is made; determines fields present in response. | [optional]
 **page** | **int**| Current page of the collection. Default 1 | [optional]
 **per_page** | **int**| Maximum number of items to be returned in result set. Default 10 | [optional]
 **search** | **string**| Limit results to those matching a string. | [optional]
 **after** | **string**| Limit response to resources published after a given ISO8601 compliant date. | [optional]
 **author** | **string**| Limit result set to posts assigned to specific authors. | [optional]
 **author_exclude** | **string**| Ensure result set excludes posts assigned to specific authors. | [optional]
 **before** | **string**| Limit response to resources published before a given ISO8601 compliant date. | [optional]
 **exclude** | **string**| Ensure result set excludes specific ids. | [optional]
 **include** | **string**| Ensure result set includes specific ids. | [optional]
 **menu_order** | **int**| The order of the object in relation to other object of its type. | [optional]
 **offset** | **string**| Offset the result set by a specific number of items. | [optional]
 **order** | **string**| Order sort attribute ascending or descending. Default desc | [optional]
 **orderby** | **string**| Sort collection by object attribute. Default date | [optional]
 **parent** | **int**| The id for the parent of the object. | [optional]
 **parent_exclude** | **string**| Ensure result set excludes specific ids. | [optional]
 **slug** | **string**| Limit result set to posts with a specific slug. | [optional]
 **status** | **string**| Limit result set to posts assigned a specific status.Default publish | [optional]
 **filter** | **string**| Use WP Query arguments to modify the response; private query vars require appropriate authorization. | [optional]

### Return type

[**\Swagger\Client\Model\Page[]**](../Model/Page.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **pagesIdDelete**
> pagesIdDelete($id, $force)

Delete Single Page

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\DefaultApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = "id_example"; // string | Id of object
$force = true; // bool | Whether to bypass trash and force deletion.

try {
    $apiInstance->pagesIdDelete($id, $force);
} catch (Exception $e) {
    echo 'Exception when calling DefaultApi->pagesIdDelete: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Id of object |
 **force** | **bool**| Whether to bypass trash and force deletion. | [optional]

### Return type

void (empty response body)

### Authorization

[oauth](../../README.md#oauth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **pagesIdGet**
> \Swagger\Client\Model\Page pagesIdGet($id, $context)

Get Single Page

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\DefaultApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = "id_example"; // string | Id of object
$context = "context_example"; // string | Scope under which the request is made; determines fields present in response.

try {
    $result = $apiInstance->pagesIdGet($id, $context);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DefaultApi->pagesIdGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Id of object |
 **context** | **string**| Scope under which the request is made; determines fields present in response. | [optional]

### Return type

[**\Swagger\Client\Model\Page**](../Model/Page.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **pagesIdPost**
> \Swagger\Client\Model\Page pagesIdPost($id, $date, $date_gmt, $password, $slug, $status, $parent, $title, $content, $author, $excerpt, $featured_media, $comment_status, $ping_status, $menu_order, $template)

Update Single Page

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\DefaultApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = "id_example"; // string | Id of object
$date = new \DateTime("2013-10-20T19:20:30+01:00"); // \DateTime | The date the object was published, in the site's timezone.
$date_gmt = new \DateTime("2013-10-20T19:20:30+01:00"); // \DateTime | The date the object was published, as GMT.
$password = "password_example"; // string | The A password to protect access to the post.
$slug = "slug_example"; // string | Limit result set to posts with a specific slug.
$status = "status_example"; // string | Limit result set to posts assigned a specific status.Default publish
$parent = 56; // int | The id for the parent of the object.
$title = "title_example"; // string | The title for the object.
$content = "content_example"; // string | The content for the object.
$author = "author_example"; // string | Limit result set to posts assigned to specific authors.
$excerpt = "excerpt_example"; // string | The excerpt for the object
$featured_media = "featured_media_example"; // string | The id of the featured media for the object.
$comment_status = "comment_status_example"; // string | Whether or not comments are open on the object
$ping_status = "ping_status_example"; // string | Whether or not the object can be pinged.
$menu_order = 56; // int | The order of the object in relation to other object of its type.
$template = 56; // int | The theme file to use to display the object.

try {
    $result = $apiInstance->pagesIdPost($id, $date, $date_gmt, $password, $slug, $status, $parent, $title, $content, $author, $excerpt, $featured_media, $comment_status, $ping_status, $menu_order, $template);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DefaultApi->pagesIdPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Id of object |
 **date** | **\DateTime**| The date the object was published, in the site&#39;s timezone. | [optional]
 **date_gmt** | **\DateTime**| The date the object was published, as GMT. | [optional]
 **password** | **string**| The A password to protect access to the post. | [optional]
 **slug** | **string**| Limit result set to posts with a specific slug. | [optional]
 **status** | **string**| Limit result set to posts assigned a specific status.Default publish | [optional]
 **parent** | **int**| The id for the parent of the object. | [optional]
 **title** | **string**| The title for the object. | [optional]
 **content** | **string**| The content for the object. | [optional]
 **author** | **string**| Limit result set to posts assigned to specific authors. | [optional]
 **excerpt** | **string**| The excerpt for the object | [optional]
 **featured_media** | **string**| The id of the featured media for the object. | [optional]
 **comment_status** | **string**| Whether or not comments are open on the object | [optional]
 **ping_status** | **string**| Whether or not the object can be pinged. | [optional]
 **menu_order** | **int**| The order of the object in relation to other object of its type. | [optional]
 **template** | **int**| The theme file to use to display the object. | [optional]

### Return type

[**\Swagger\Client\Model\Page**](../Model/Page.md)

### Authorization

[oauth](../../README.md#oauth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **pagesPost**
> \Swagger\Client\Model\Page pagesPost($date, $date_gmt, $password, $slug, $status, $parent, $title, $content, $author, $excerpt, $featured_media, $comment_status, $ping_status, $menu_order, $template)

Create Page

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\DefaultApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$date = new \DateTime("2013-10-20T19:20:30+01:00"); // \DateTime | The date the object was published, in the site's timezone.
$date_gmt = new \DateTime("2013-10-20T19:20:30+01:00"); // \DateTime | The date the object was published, as GMT.
$password = "password_example"; // string | The A password to protect access to the post.
$slug = "slug_example"; // string | Limit result set to posts with a specific slug.
$status = "status_example"; // string | Limit result set to posts assigned a specific status.Default publish
$parent = 56; // int | The id for the parent of the object.
$title = "title_example"; // string | The title for the object.
$content = "content_example"; // string | The content for the object.
$author = "author_example"; // string | Limit result set to posts assigned to specific authors.
$excerpt = "excerpt_example"; // string | The excerpt for the object
$featured_media = "featured_media_example"; // string | The id of the featured media for the object.
$comment_status = "comment_status_example"; // string | Whether or not comments are open on the object
$ping_status = "ping_status_example"; // string | Whether or not the object can be pinged.
$menu_order = 56; // int | The order of the object in relation to other object of its type.
$template = 56; // int | The theme file to use to display the object.

try {
    $result = $apiInstance->pagesPost($date, $date_gmt, $password, $slug, $status, $parent, $title, $content, $author, $excerpt, $featured_media, $comment_status, $ping_status, $menu_order, $template);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DefaultApi->pagesPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **date** | **\DateTime**| The date the object was published, in the site&#39;s timezone. | [optional]
 **date_gmt** | **\DateTime**| The date the object was published, as GMT. | [optional]
 **password** | **string**| The A password to protect access to the post. | [optional]
 **slug** | **string**| Limit result set to posts with a specific slug. | [optional]
 **status** | **string**| Limit result set to posts assigned a specific status.Default publish | [optional]
 **parent** | **int**| The id for the parent of the object. | [optional]
 **title** | **string**| The title for the object. | [optional]
 **content** | **string**| The content for the object. | [optional]
 **author** | **string**| Limit result set to posts assigned to specific authors. | [optional]
 **excerpt** | **string**| The excerpt for the object | [optional]
 **featured_media** | **string**| The id of the featured media for the object. | [optional]
 **comment_status** | **string**| Whether or not comments are open on the object | [optional]
 **ping_status** | **string**| Whether or not the object can be pinged. | [optional]
 **menu_order** | **int**| The order of the object in relation to other object of its type. | [optional]
 **template** | **int**| The theme file to use to display the object. | [optional]

### Return type

[**\Swagger\Client\Model\Page**](../Model/Page.md)

### Authorization

[oauth](../../README.md#oauth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **postsGet**
> \Swagger\Client\Model\Post[] postsGet($context, $page, $per_page, $search, $after, $author, $author_exclude, $before, $exclude, $include, $offset, $order, $orderby, $slug, $status, $filter, $categories, $tags)

List Posts

Scope under which the request is made; determines fields present in response.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\DefaultApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$context = "context_example"; // string | Scope under which the request is made; determines fields present in response.
$page = 56; // int | Current page of the collection. Default 1
$per_page = 56; // int | Maximum number of items to be returned in result set. Default 10
$search = "search_example"; // string | Limit results to those matching a string.
$after = "after_example"; // string | Limit response to resources published after a given ISO8601 compliant date.
$author = "author_example"; // string | Limit result set to posts assigned to specific authors.
$author_exclude = "author_exclude_example"; // string | Ensure result set excludes posts assigned to specific authors.
$before = "before_example"; // string | Limit response to resources published before a given ISO8601 compliant date.
$exclude = "exclude_example"; // string | Ensure result set excludes specific ids.
$include = "include_example"; // string | Ensure result set includes specific ids.
$offset = "offset_example"; // string | Offset the result set by a specific number of items.
$order = "order_example"; // string | Order sort attribute ascending or descending. Default desc
$orderby = "orderby_example"; // string | Sort collection by object attribute. Default date
$slug = "slug_example"; // string | Limit result set to posts with a specific slug.
$status = "status_example"; // string | Limit result set to posts assigned a specific status.Default publish
$filter = "filter_example"; // string | Use WP Query arguments to modify the response; private query vars require appropriate authorization.
$categories = array("categories_example"); // string[] | Limit result set to all items that have the specified term assigned in the categories taxonomy.
$tags = array("tags_example"); // string[] | Limit result set to all items that have the specified term assigned in the tags taxonomy.

try {
    $result = $apiInstance->postsGet($context, $page, $per_page, $search, $after, $author, $author_exclude, $before, $exclude, $include, $offset, $order, $orderby, $slug, $status, $filter, $categories, $tags);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DefaultApi->postsGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **context** | **string**| Scope under which the request is made; determines fields present in response. | [optional]
 **page** | **int**| Current page of the collection. Default 1 | [optional]
 **per_page** | **int**| Maximum number of items to be returned in result set. Default 10 | [optional]
 **search** | **string**| Limit results to those matching a string. | [optional]
 **after** | **string**| Limit response to resources published after a given ISO8601 compliant date. | [optional]
 **author** | **string**| Limit result set to posts assigned to specific authors. | [optional]
 **author_exclude** | **string**| Ensure result set excludes posts assigned to specific authors. | [optional]
 **before** | **string**| Limit response to resources published before a given ISO8601 compliant date. | [optional]
 **exclude** | **string**| Ensure result set excludes specific ids. | [optional]
 **include** | **string**| Ensure result set includes specific ids. | [optional]
 **offset** | **string**| Offset the result set by a specific number of items. | [optional]
 **order** | **string**| Order sort attribute ascending or descending. Default desc | [optional]
 **orderby** | **string**| Sort collection by object attribute. Default date | [optional]
 **slug** | **string**| Limit result set to posts with a specific slug. | [optional]
 **status** | **string**| Limit result set to posts assigned a specific status.Default publish | [optional]
 **filter** | **string**| Use WP Query arguments to modify the response; private query vars require appropriate authorization. | [optional]
 **categories** | [**string[]**](../Model/string.md)| Limit result set to all items that have the specified term assigned in the categories taxonomy. | [optional]
 **tags** | [**string[]**](../Model/string.md)| Limit result set to all items that have the specified term assigned in the tags taxonomy. | [optional]

### Return type

[**\Swagger\Client\Model\Post[]**](../Model/Post.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **postsIdDelete**
> postsIdDelete($id, $force)

Delete Single Post

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\DefaultApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = "id_example"; // string | Id of object
$force = true; // bool | Whether to bypass trash and force deletion.

try {
    $apiInstance->postsIdDelete($id, $force);
} catch (Exception $e) {
    echo 'Exception when calling DefaultApi->postsIdDelete: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Id of object |
 **force** | **bool**| Whether to bypass trash and force deletion. | [optional]

### Return type

void (empty response body)

### Authorization

[oauth](../../README.md#oauth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **postsIdGet**
> \Swagger\Client\Model\Post postsIdGet($id, $context)

Get Single Post

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\DefaultApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = "id_example"; // string | Id of object
$context = "context_example"; // string | Scope under which the request is made; determines fields present in response.

try {
    $result = $apiInstance->postsIdGet($id, $context);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DefaultApi->postsIdGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Id of object |
 **context** | **string**| Scope under which the request is made; determines fields present in response. | [optional]

### Return type

[**\Swagger\Client\Model\Post**](../Model/Post.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **postsIdPost**
> \Swagger\Client\Model\Post postsIdPost($id, $context, $page, $per_page, $search, $after, $author, $author_exclude, $before, $exclude, $include, $offset, $order, $orderby, $slug, $status, $filter, $categories, $tags)

Update Single Post

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\DefaultApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = "id_example"; // string | Id of object
$context = "context_example"; // string | Scope under which the request is made; determines fields present in response.
$page = 56; // int | Current page of the collection. Default 1
$per_page = 56; // int | Maximum number of items to be returned in result set. Default 10
$search = "search_example"; // string | Limit results to those matching a string.
$after = "after_example"; // string | Limit response to resources published after a given ISO8601 compliant date.
$author = "author_example"; // string | Limit result set to posts assigned to specific authors.
$author_exclude = "author_exclude_example"; // string | Ensure result set excludes posts assigned to specific authors.
$before = "before_example"; // string | Limit response to resources published before a given ISO8601 compliant date.
$exclude = "exclude_example"; // string | Ensure result set excludes specific ids.
$include = "include_example"; // string | Ensure result set includes specific ids.
$offset = "offset_example"; // string | Offset the result set by a specific number of items.
$order = "order_example"; // string | Order sort attribute ascending or descending. Default desc
$orderby = "orderby_example"; // string | Sort collection by object attribute. Default date
$slug = "slug_example"; // string | Limit result set to posts with a specific slug.
$status = "status_example"; // string | Limit result set to posts assigned a specific status.Default publish
$filter = "filter_example"; // string | Use WP Query arguments to modify the response; private query vars require appropriate authorization.
$categories = array("categories_example"); // string[] | Limit result set to all items that have the specified term assigned in the categories taxonomy.
$tags = array("tags_example"); // string[] | Limit result set to all items that have the specified term assigned in the tags taxonomy.

try {
    $result = $apiInstance->postsIdPost($id, $context, $page, $per_page, $search, $after, $author, $author_exclude, $before, $exclude, $include, $offset, $order, $orderby, $slug, $status, $filter, $categories, $tags);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DefaultApi->postsIdPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Id of object |
 **context** | **string**| Scope under which the request is made; determines fields present in response. | [optional]
 **page** | **int**| Current page of the collection. Default 1 | [optional]
 **per_page** | **int**| Maximum number of items to be returned in result set. Default 10 | [optional]
 **search** | **string**| Limit results to those matching a string. | [optional]
 **after** | **string**| Limit response to resources published after a given ISO8601 compliant date. | [optional]
 **author** | **string**| Limit result set to posts assigned to specific authors. | [optional]
 **author_exclude** | **string**| Ensure result set excludes posts assigned to specific authors. | [optional]
 **before** | **string**| Limit response to resources published before a given ISO8601 compliant date. | [optional]
 **exclude** | **string**| Ensure result set excludes specific ids. | [optional]
 **include** | **string**| Ensure result set includes specific ids. | [optional]
 **offset** | **string**| Offset the result set by a specific number of items. | [optional]
 **order** | **string**| Order sort attribute ascending or descending. Default desc | [optional]
 **orderby** | **string**| Sort collection by object attribute. Default date | [optional]
 **slug** | **string**| Limit result set to posts with a specific slug. | [optional]
 **status** | **string**| Limit result set to posts assigned a specific status.Default publish | [optional]
 **filter** | **string**| Use WP Query arguments to modify the response; private query vars require appropriate authorization. | [optional]
 **categories** | [**string[]**](../Model/string.md)| Limit result set to all items that have the specified term assigned in the categories taxonomy. | [optional]
 **tags** | [**string[]**](../Model/string.md)| Limit result set to all items that have the specified term assigned in the tags taxonomy. | [optional]

### Return type

[**\Swagger\Client\Model\Post**](../Model/Post.md)

### Authorization

[oauth](../../README.md#oauth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **postsIdRevisionsGet**
> \Swagger\Client\Model\Revision[] postsIdRevisionsGet($id, $context)

Get post revisions

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\DefaultApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = "id_example"; // string | Id of object
$context = "context_example"; // string | Scope under which the request is made; determines fields present in response.

try {
    $result = $apiInstance->postsIdRevisionsGet($id, $context);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DefaultApi->postsIdRevisionsGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Id of object |
 **context** | **string**| Scope under which the request is made; determines fields present in response. | [optional]

### Return type

[**\Swagger\Client\Model\Revision[]**](../Model/Revision.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **postsIdRevisionsRevisionidDelete**
> postsIdRevisionsRevisionidDelete($id, $revisionid)

Delete single post revisions

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\DefaultApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = "id_example"; // string | Id of object
$revisionid = "revisionid_example"; // string | Id of revision

try {
    $apiInstance->postsIdRevisionsRevisionidDelete($id, $revisionid);
} catch (Exception $e) {
    echo 'Exception when calling DefaultApi->postsIdRevisionsRevisionidDelete: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Id of object |
 **revisionid** | **string**| Id of revision |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **postsIdRevisionsRevisionidGet**
> \Swagger\Client\Model\Revision postsIdRevisionsRevisionidGet($id, $revisionid, $context)

Get single post revisions

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\DefaultApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = "id_example"; // string | Id of object
$revisionid = "revisionid_example"; // string | Id of revision
$context = "context_example"; // string | Scope under which the request is made; determines fields present in response.

try {
    $result = $apiInstance->postsIdRevisionsRevisionidGet($id, $revisionid, $context);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DefaultApi->postsIdRevisionsRevisionidGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Id of object |
 **revisionid** | **string**| Id of revision |
 **context** | **string**| Scope under which the request is made; determines fields present in response. | [optional]

### Return type

[**\Swagger\Client\Model\Revision**](../Model/Revision.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **postsPost**
> \Swagger\Client\Model\Post postsPost($date, $date_gmt, $password, $slug, $status, $title, $content, $author, $excerpt, $featured_media, $comment_status, $ping_status, $sticky, $categories, $tags)

Create Post

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\DefaultApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$date = new \DateTime("2013-10-20T19:20:30+01:00"); // \DateTime | The date the object was published, in the site's timezone.
$date_gmt = new \DateTime("2013-10-20T19:20:30+01:00"); // \DateTime | The date the object was published, as GMT.
$password = "password_example"; // string | The A password to protect access to the post.
$slug = "slug_example"; // string | Limit result set to posts with a specific slug.
$status = "status_example"; // string | Limit result set to posts assigned a specific status.Default publish
$title = "title_example"; // string | The title for the object.
$content = "content_example"; // string | The content for the object.
$author = "author_example"; // string | Limit result set to posts assigned to specific authors.
$excerpt = "excerpt_example"; // string | The excerpt for the object
$featured_media = "featured_media_example"; // string | The id of the featured media for the object.
$comment_status = "comment_status_example"; // string | Whether or not comments are open on the object
$ping_status = "ping_status_example"; // string | Whether or not the object can be pinged.
$sticky = true; // bool | Whether or not the object should be treated as sticky.
$categories = array("categories_example"); // string[] | Limit result set to all items that have the specified term assigned in the categories taxonomy.
$tags = array("tags_example"); // string[] | Limit result set to all items that have the specified term assigned in the tags taxonomy.

try {
    $result = $apiInstance->postsPost($date, $date_gmt, $password, $slug, $status, $title, $content, $author, $excerpt, $featured_media, $comment_status, $ping_status, $sticky, $categories, $tags);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DefaultApi->postsPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **date** | **\DateTime**| The date the object was published, in the site&#39;s timezone. | [optional]
 **date_gmt** | **\DateTime**| The date the object was published, as GMT. | [optional]
 **password** | **string**| The A password to protect access to the post. | [optional]
 **slug** | **string**| Limit result set to posts with a specific slug. | [optional]
 **status** | **string**| Limit result set to posts assigned a specific status.Default publish | [optional]
 **title** | **string**| The title for the object. | [optional]
 **content** | **string**| The content for the object. | [optional]
 **author** | **string**| Limit result set to posts assigned to specific authors. | [optional]
 **excerpt** | **string**| The excerpt for the object | [optional]
 **featured_media** | **string**| The id of the featured media for the object. | [optional]
 **comment_status** | **string**| Whether or not comments are open on the object | [optional]
 **ping_status** | **string**| Whether or not the object can be pinged. | [optional]
 **sticky** | **bool**| Whether or not the object should be treated as sticky. | [optional]
 **categories** | [**string[]**](../Model/string.md)| Limit result set to all items that have the specified term assigned in the categories taxonomy. | [optional]
 **tags** | [**string[]**](../Model/string.md)| Limit result set to all items that have the specified term assigned in the tags taxonomy. | [optional]

### Return type

[**\Swagger\Client\Model\Post**](../Model/Post.md)

### Authorization

[oauth](../../README.md#oauth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **statusesGet**
> \Swagger\Client\Model\Status[] statusesGet($context)

List Status

Scope under which the request is made; determines fields present in response.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\DefaultApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$context = "context_example"; // string | Scope under which the request is made; determines fields present in response.

try {
    $result = $apiInstance->statusesGet($context);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DefaultApi->statusesGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **context** | **string**| Scope under which the request is made; determines fields present in response. | [optional]

### Return type

[**\Swagger\Client\Model\Status[]**](../Model/Status.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **statusesIdGet**
> \Swagger\Client\Model\Status statusesIdGet($id, $context)

Get Single Status

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\DefaultApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = "id_example"; // string | Id of object
$context = "context_example"; // string | Scope under which the request is made; determines fields present in response.

try {
    $result = $apiInstance->statusesIdGet($id, $context);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DefaultApi->statusesIdGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Id of object |
 **context** | **string**| Scope under which the request is made; determines fields present in response. | [optional]

### Return type

[**\Swagger\Client\Model\Status**](../Model/Status.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **tagsGet**
> \Swagger\Client\Model\Tag[] tagsGet($context, $page, $per_page, $search, $exclude, $include, $order, $orderby, $post, $slug)

List Tags

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\DefaultApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$context = "context_example"; // string | Scope under which the request is made; determines fields present in response.
$page = 56; // int | Current page of the collection. Default 1
$per_page = 56; // int | Maximum number of items to be returned in result set. Default 10
$search = "search_example"; // string | Limit results to those matching a string.
$exclude = "exclude_example"; // string | Ensure result set excludes specific ids.
$include = "include_example"; // string | Ensure result set includes specific ids.
$order = "order_example"; // string | Order sort attribute ascending or descending. Default desc
$orderby = "orderby_example"; // string | Sort collection by object attribute. Default date
$post = "post_example"; // string | The id for the associated post of the resource.
$slug = "slug_example"; // string | Limit result set to posts with a specific slug.

try {
    $result = $apiInstance->tagsGet($context, $page, $per_page, $search, $exclude, $include, $order, $orderby, $post, $slug);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DefaultApi->tagsGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **context** | **string**| Scope under which the request is made; determines fields present in response. | [optional]
 **page** | **int**| Current page of the collection. Default 1 | [optional]
 **per_page** | **int**| Maximum number of items to be returned in result set. Default 10 | [optional]
 **search** | **string**| Limit results to those matching a string. | [optional]
 **exclude** | **string**| Ensure result set excludes specific ids. | [optional]
 **include** | **string**| Ensure result set includes specific ids. | [optional]
 **order** | **string**| Order sort attribute ascending or descending. Default desc | [optional]
 **orderby** | **string**| Sort collection by object attribute. Default date | [optional]
 **post** | **string**| The id for the associated post of the resource. | [optional]
 **slug** | **string**| Limit result set to posts with a specific slug. | [optional]

### Return type

[**\Swagger\Client\Model\Tag[]**](../Model/Tag.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **tagsIdDelete**
> tagsIdDelete($id, $force)

Delete Single Tag

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\DefaultApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = "id_example"; // string | Id of object
$force = true; // bool | Whether to bypass trash and force deletion.

try {
    $apiInstance->tagsIdDelete($id, $force);
} catch (Exception $e) {
    echo 'Exception when calling DefaultApi->tagsIdDelete: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Id of object |
 **force** | **bool**| Whether to bypass trash and force deletion. | [optional]

### Return type

void (empty response body)

### Authorization

[oauth](../../README.md#oauth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **tagsIdGet**
> \Swagger\Client\Model\Tag tagsIdGet($id, $context)

Get Single Tag

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\DefaultApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = "id_example"; // string | Id of object
$context = "context_example"; // string | Scope under which the request is made; determines fields present in response.

try {
    $result = $apiInstance->tagsIdGet($id, $context);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DefaultApi->tagsIdGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Id of object |
 **context** | **string**| Scope under which the request is made; determines fields present in response. | [optional]

### Return type

[**\Swagger\Client\Model\Tag**](../Model/Tag.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **tagsIdPost**
> \Swagger\Client\Model\Tag tagsIdPost($id, $name, $description, $slug)

Update Single Tag

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\DefaultApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = "id_example"; // string | Id of object
$name = "name_example"; // string | HTML title for the resource.
$description = "description_example"; // string | The description for the resource
$slug = "slug_example"; // string | Limit result set to posts with a specific slug.

try {
    $result = $apiInstance->tagsIdPost($id, $name, $description, $slug);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DefaultApi->tagsIdPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Id of object |
 **name** | **string**| HTML title for the resource. |
 **description** | **string**| The description for the resource | [optional]
 **slug** | **string**| Limit result set to posts with a specific slug. | [optional]

### Return type

[**\Swagger\Client\Model\Tag**](../Model/Tag.md)

### Authorization

[oauth](../../README.md#oauth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **tagsPost**
> \Swagger\Client\Model\Tag tagsPost($name, $description, $slug)

Create Tag

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\DefaultApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$name = "name_example"; // string | HTML title for the resource.
$description = "description_example"; // string | The description for the resource
$slug = "slug_example"; // string | Limit result set to posts with a specific slug.

try {
    $result = $apiInstance->tagsPost($name, $description, $slug);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DefaultApi->tagsPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **string**| HTML title for the resource. |
 **description** | **string**| The description for the resource | [optional]
 **slug** | **string**| Limit result set to posts with a specific slug. | [optional]

### Return type

[**\Swagger\Client\Model\Tag**](../Model/Tag.md)

### Authorization

[oauth](../../README.md#oauth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **taxonomiesGet**
> \Swagger\Client\Model\Taxonomy[] taxonomiesGet($context, $type)

List Taxonomy

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\DefaultApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$context = "context_example"; // string | Scope under which the request is made; determines fields present in response.
$type = "type_example"; // string | Limit result set to comments assigned a specific type. Requires authorization. Default comment

try {
    $result = $apiInstance->taxonomiesGet($context, $type);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DefaultApi->taxonomiesGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **context** | **string**| Scope under which the request is made; determines fields present in response. | [optional]
 **type** | **string**| Limit result set to comments assigned a specific type. Requires authorization. Default comment | [optional]

### Return type

[**\Swagger\Client\Model\Taxonomy[]**](../Model/Taxonomy.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **taxonomiesIdGet**
> \Swagger\Client\Model\Taxonomy taxonomiesIdGet($id, $context)

Get Single Taxonomy

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\DefaultApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = "id_example"; // string | Id of object
$context = "context_example"; // string | Scope under which the request is made; determines fields present in response.

try {
    $result = $apiInstance->taxonomiesIdGet($id, $context);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DefaultApi->taxonomiesIdGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Id of object |
 **context** | **string**| Scope under which the request is made; determines fields present in response. | [optional]

### Return type

[**\Swagger\Client\Model\Taxonomy**](../Model/Taxonomy.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **typesGet**
> \Swagger\Client\Model\Type[] typesGet($context)

List Type

Scope under which the request is made; determines fields present in response.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\DefaultApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$context = "context_example"; // string | Scope under which the request is made; determines fields present in response.

try {
    $result = $apiInstance->typesGet($context);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DefaultApi->typesGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **context** | **string**| Scope under which the request is made; determines fields present in response. | [optional]

### Return type

[**\Swagger\Client\Model\Type[]**](../Model/Type.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **typesIdGet**
> \Swagger\Client\Model\Type typesIdGet($id, $context)

Get Single Type

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\DefaultApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = "id_example"; // string | Id of object
$context = "context_example"; // string | Scope under which the request is made; determines fields present in response.

try {
    $result = $apiInstance->typesIdGet($id, $context);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DefaultApi->typesIdGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Id of object |
 **context** | **string**| Scope under which the request is made; determines fields present in response. | [optional]

### Return type

[**\Swagger\Client\Model\Type**](../Model/Type.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **usersGet**
> \Swagger\Client\Model\User[] usersGet($context, $page, $per_page, $search, $exclude, $include, $order, $orderby, $roles, $slug)

List Tags

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\DefaultApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$context = "context_example"; // string | Scope under which the request is made; determines fields present in response.
$page = 56; // int | Current page of the collection. Default 1
$per_page = 56; // int | Maximum number of items to be returned in result set. Default 10
$search = "search_example"; // string | Limit results to those matching a string.
$exclude = "exclude_example"; // string | Ensure result set excludes specific ids.
$include = "include_example"; // string | Ensure result set includes specific ids.
$order = "order_example"; // string | Order sort attribute ascending or descending. Default desc
$orderby = "orderby_example"; // string | Sort collection by object attribute. Default date
$roles = array("roles_example"); // string[] | Roles assigned to the resource.
$slug = "slug_example"; // string | Limit result set to posts with a specific slug.

try {
    $result = $apiInstance->usersGet($context, $page, $per_page, $search, $exclude, $include, $order, $orderby, $roles, $slug);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DefaultApi->usersGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **context** | **string**| Scope under which the request is made; determines fields present in response. | [optional]
 **page** | **int**| Current page of the collection. Default 1 | [optional]
 **per_page** | **int**| Maximum number of items to be returned in result set. Default 10 | [optional]
 **search** | **string**| Limit results to those matching a string. | [optional]
 **exclude** | **string**| Ensure result set excludes specific ids. | [optional]
 **include** | **string**| Ensure result set includes specific ids. | [optional]
 **order** | **string**| Order sort attribute ascending or descending. Default desc | [optional]
 **orderby** | **string**| Sort collection by object attribute. Default date | [optional]
 **roles** | [**string[]**](../Model/string.md)| Roles assigned to the resource. | [optional]
 **slug** | **string**| Limit result set to posts with a specific slug. | [optional]

### Return type

[**\Swagger\Client\Model\User[]**](../Model/User.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **usersIdDelete**
> usersIdDelete($id, $force, $reassign)

Delete Single User

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\DefaultApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = "id_example"; // string | Id of object
$force = true; // bool | Whether to bypass trash and force deletion.
$reassign = "reassign_example"; // string | 

try {
    $apiInstance->usersIdDelete($id, $force, $reassign);
} catch (Exception $e) {
    echo 'Exception when calling DefaultApi->usersIdDelete: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Id of object |
 **force** | **bool**| Whether to bypass trash and force deletion. | [optional]
 **reassign** | **string**|  | [optional]

### Return type

void (empty response body)

### Authorization

[oauth](../../README.md#oauth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **usersIdGet**
> \Swagger\Client\Model\User usersIdGet($id, $context)

Get Single User

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\DefaultApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = "id_example"; // string | Id of object
$context = "context_example"; // string | Scope under which the request is made; determines fields present in response.

try {
    $result = $apiInstance->usersIdGet($id, $context);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DefaultApi->usersIdGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Id of object |
 **context** | **string**| Scope under which the request is made; determines fields present in response. | [optional]

### Return type

[**\Swagger\Client\Model\User**](../Model/User.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **usersIdPost**
> \Swagger\Client\Model\User usersIdPost($id, $name, $username, $first_name, $last_name, $email, $url, $description, $nickname, $slug, $roles, $password, $capabilities)

Update Single User

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\DefaultApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = "id_example"; // string | Id of object
$name = "name_example"; // string | HTML title for the resource.
$username = "username_example"; // string | The user name for the resource.
$first_name = "first_name_example"; // string | The first name for the resource.
$last_name = "last_name_example"; // string | The last name for the resource.
$email = "email_example"; // string | Email of the resource.
$url = "url_example"; // string | URL of the resource.
$description = "description_example"; // string | The description for the resource
$nickname = "nickname_example"; // string | The nickname for the resource.
$slug = "slug_example"; // string | Limit result set to posts with a specific slug.
$roles = array("roles_example"); // string[] | Roles assigned to the resource.
$password = "password_example"; // string | The A password to protect access to the post.
$capabilities = array("capabilities_example"); // string[] | All capabilities used by the resource.

try {
    $result = $apiInstance->usersIdPost($id, $name, $username, $first_name, $last_name, $email, $url, $description, $nickname, $slug, $roles, $password, $capabilities);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DefaultApi->usersIdPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Id of object |
 **name** | **string**| HTML title for the resource. |
 **username** | **string**| The user name for the resource. | [optional]
 **first_name** | **string**| The first name for the resource. | [optional]
 **last_name** | **string**| The last name for the resource. | [optional]
 **email** | **string**| Email of the resource. | [optional]
 **url** | **string**| URL of the resource. | [optional]
 **description** | **string**| The description for the resource | [optional]
 **nickname** | **string**| The nickname for the resource. | [optional]
 **slug** | **string**| Limit result set to posts with a specific slug. | [optional]
 **roles** | [**string[]**](../Model/string.md)| Roles assigned to the resource. | [optional]
 **password** | **string**| The A password to protect access to the post. | [optional]
 **capabilities** | [**string[]**](../Model/string.md)| All capabilities used by the resource. | [optional]

### Return type

[**\Swagger\Client\Model\User**](../Model/User.md)

### Authorization

[oauth](../../README.md#oauth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **usersPost**
> \Swagger\Client\Model\User usersPost($name, $username, $first_name, $last_name, $email, $url, $description, $nickname, $slug, $roles, $password, $capabilities)

Create User

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\DefaultApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$name = "name_example"; // string | HTML title for the resource.
$username = "username_example"; // string | The user name for the resource.
$first_name = "first_name_example"; // string | The first name for the resource.
$last_name = "last_name_example"; // string | The last name for the resource.
$email = "email_example"; // string | Email of the resource.
$url = "url_example"; // string | URL of the resource.
$description = "description_example"; // string | The description for the resource
$nickname = "nickname_example"; // string | The nickname for the resource.
$slug = "slug_example"; // string | Limit result set to posts with a specific slug.
$roles = array("roles_example"); // string[] | Roles assigned to the resource.
$password = "password_example"; // string | The A password to protect access to the post.
$capabilities = array("capabilities_example"); // string[] | All capabilities used by the resource.

try {
    $result = $apiInstance->usersPost($name, $username, $first_name, $last_name, $email, $url, $description, $nickname, $slug, $roles, $password, $capabilities);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DefaultApi->usersPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **string**| HTML title for the resource. |
 **username** | **string**| The user name for the resource. | [optional]
 **first_name** | **string**| The first name for the resource. | [optional]
 **last_name** | **string**| The last name for the resource. | [optional]
 **email** | **string**| Email of the resource. | [optional]
 **url** | **string**| URL of the resource. | [optional]
 **description** | **string**| The description for the resource | [optional]
 **nickname** | **string**| The nickname for the resource. | [optional]
 **slug** | **string**| Limit result set to posts with a specific slug. | [optional]
 **roles** | [**string[]**](../Model/string.md)| Roles assigned to the resource. | [optional]
 **password** | **string**| The A password to protect access to the post. | [optional]
 **capabilities** | [**string[]**](../Model/string.md)| All capabilities used by the resource. | [optional]

### Return type

[**\Swagger\Client\Model\User**](../Model/User.md)

### Authorization

[oauth](../../README.md#oauth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

