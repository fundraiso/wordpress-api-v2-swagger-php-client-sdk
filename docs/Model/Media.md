# Media

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**date** | [**\DateTime**](\DateTime.md) | The date the object was published, in the site&#39;s timezone. | [optional] 
**date_gmt** | [**\DateTime**](\DateTime.md) | The date the object was published, as GMT. | [optional] 
**guid** | **string** | The globally unique identifier for the object. | [optional] 
**id** | **int** | Unique identifier for the object. | [optional] 
**modified** | [**\DateTime**](\DateTime.md) | The date the object was last modified, in the site&#39;s timezone. | [optional] 
**modified_gmt** | [**\DateTime**](\DateTime.md) | The date the object was last modified, as GMT. | [optional] 
**password** | **string** | The A password to protect access to the post. | [optional] 
**slug** | **string** | An alphanumeric identifier for the object unique to its type. | [optional] 
**status** | **string** | A named status for the object. | [optional] 
**type** | **string** | Type of Post for the object. | [optional] 
**title** | **string** | The title for the object. | [optional] 
**author** | **string** | The id for the author of the object. | [optional] 
**comment_status** | **string** | Whether or not comments are open on the object | [optional] 
**ping_status** | **string** | Whether or not the object can be pinged. | [optional] 
**alt_text** | **string** | Alternative text to display when resource is not displayed | [optional] 
**caption** | **string** | The caption for the resource. | [optional] 
**description** | **string** | The description for the resource. | [optional] 
**media_type** | **string** | Type of resource. | [optional] 
**mime_type** | **string** | Mime type of resource. | [optional] 
**media_details** | **string** | Details about the resource file, specific to its type. | [optional] 
**post** | **string** | The id for the associated post of the resource. | [optional] 
**source_url** | **string** | URL to the original resource file. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


