# Tag

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Unique identifier for the object. | [optional] 
**count** | **int** | Number of published posts for the resource. | [optional] 
**description** | **string** | The description for the resource. | [optional] 
**link** | **string** | URL to the object. | [optional] 
**name** | **string** | The title for the resource. | [optional] 
**slug** | **string** | An alphanumeric identifier for the object unique to its type. | [optional] 
**taxonomy** | **string** | Type attribution for the resource. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


