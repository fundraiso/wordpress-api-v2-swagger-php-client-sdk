# CommonPostPage

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**date** | [**\DateTime**](\DateTime.md) | The date the object was published, in the site&#39;s timezone. | [optional] 
**date_gmt** | [**\DateTime**](\DateTime.md) | The date the object was published, as GMT. | [optional] 
**guid** | **string** | The globally unique identifier for the object. | [optional] 
**id** | **int** | Unique identifier for the object. | [optional] 
**link** | **string** | URL to the object. | [optional] 
**modified** | [**\DateTime**](\DateTime.md) | The date the object was last modified, in the site&#39;s timezone. | [optional] 
**modified_gmt** | [**\DateTime**](\DateTime.md) | The date the object was last modified, as GMT. | [optional] 
**password** | **string** | The A password to protect access to the post. | [optional] 
**slug** | **string** | An alphanumeric identifier for the object unique to its type. | [optional] 
**status** | **string** | A named status for the object. | [optional] 
**type** | **string** | Type of Post for the object. | [optional] 
**title** | **string** | The title for the object. | [optional] 
**content** | **string** | The content for the object. | [optional] 
**author** | **string** | The id for the author of the object. | [optional] 
**excerpt** | **string** | The excerpt for the object | [optional] 
**featured_media** | **string** | The id of the featured media for the object. | [optional] 
**comment_status** | **string** | Whether or not comments are open on the object | [optional] 
**ping_status** | **string** | Whether or not the object can be pinged. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


