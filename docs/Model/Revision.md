# Revision

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**date** | [**\DateTime**](\DateTime.md) | The date the object was published, in the site&#39;s timezone. | [optional] 
**date_gmt** | [**\DateTime**](\DateTime.md) | The date the object was published, as GMT. | [optional] 
**guid** | **string** | The globally unique identifier for the object. | [optional] 
**id** | **int** | Unique identifier for the object. | [optional] 
**modified** | [**\DateTime**](\DateTime.md) | The date the object was last modified, in the site&#39;s timezone. | [optional] 
**modified_gmt** | [**\DateTime**](\DateTime.md) | The date the object was last modified, as GMT. | [optional] 
**slug** | **string** | An alphanumeric identifier for the object unique to its type. | [optional] 
**title** | **string** | The title for the object. | [optional] 
**content** | **string** | The content for the object. | [optional] 
**author** | **string** | The id for the author of the object. | [optional] 
**excerpt** | **string** | The excerpt for the object | [optional] 
**parent** | **int** | The id for the parent of the object. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


