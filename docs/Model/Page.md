# Page

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**parent** | **int** | The id for the parent of the object. | [optional] 
**menu_order** | **int** | The order of the object in relation to other object of its type. | [optional] 
**template** | **int** | The theme file to use to display the object. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


