# Comment

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Unique identifier for the object. | [optional] 
**author** | **string** | The id for the author of the object. | [optional] 
**author_email** | **string** | Email address for the object author. | [optional] 
**author_ip** | **string** | IP address for the object author. | [optional] 
**author_name** | **string** | Display name for the object author. | [optional] 
**author_url** | **string** | URL for the object author. | [optional] 
**author_user_agent** | **string** | User agent for the object author. | [optional] 
**content** | **string** | The content for the object. | [optional] 
**date** | [**\DateTime**](\DateTime.md) | The date the object was published, in the site&#39;s timezone. | [optional] 
**date_gmt** | [**\DateTime**](\DateTime.md) | The date the object was published, as GMT. | [optional] 
**karma** | **string** | Karma for the object | [optional] 
**link** | **string** | URL to the object. | [optional] 
**parent** | **int** | The id for the parent of the object. | [optional] 
**post** | **string** | The id for the associated post of the resource. | [optional] 
**status** | **string** | A named status for the object. | [optional] 
**type** | **string** | Type of Post for the object. | [optional] 
**author_avatar_urls** | **string** | Avatar URLs for the object author. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


