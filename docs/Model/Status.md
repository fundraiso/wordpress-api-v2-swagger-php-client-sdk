# Status

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**private** | **bool** | Whether posts with this resource should be private. | [optional] 
**protected** | **bool** | Whether posts with this resource should be protected. | [optional] 
**public** | **bool** | Whether posts of this resource should be shown in the front end of the site. | [optional] 
**queryable** | **bool** | Whether posts with this resource should be publicly-queryable. | [optional] 
**show_in_list** | **bool** | Whether to include posts in the edit listing for their post type. | [optional] 
**name** | **string** | The title for the resource. | [optional] 
**slug** | **string** | An alphanumeric identifier for the object unique to its type. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


