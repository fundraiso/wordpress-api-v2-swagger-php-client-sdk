# Type

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**capabilities** | **string[]** | All capabilities used by the resource. | [optional] 
**description** | **string** | A human-readable description of the resource. | [optional] 
**hierarchical** | **string** | Whether or not the resource should have children. | [optional] 
**labels** | **string** | Human-readable labels for the resource for various contexts. | [optional] 
**name** | **string** | The title for the resource. | [optional] 
**slug** | **string** | An alphanumeric identifier for the object unique to its type. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


