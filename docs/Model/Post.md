# Post

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**format** | **string** | The format for the object. | [optional] 
**sticky** | **bool** | Whether or not the object should be treated as sticky. | [optional] 
**categories** | **string[]** | The terms assigned to the object in the category taxonomy. | [optional] 
**tags** | **string[]** | he terms assigned to the object in the post_tag taxonomy. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


