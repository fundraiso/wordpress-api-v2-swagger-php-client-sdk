# Taxonomy

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**show_cloud** | **bool** | Whether or not the term cloud should be displayed. | [optional] 
**types** | **string[]** | Types associated with resource. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


