# User

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Unique identifier for the object. | [optional] 
**username** | **string** | Login name for the resource. | [optional] 
**name** | **string** | Display name for the resource. | [optional] 
**first_name** | **string** | First name for the resource. | [optional] 
**last_name** | **string** | Last name for the resource. | [optional] 
**email** | **string** | The email address for the resource. | [optional] 
**url** | **string** | URL of the resource. | [optional] 
**description** | **string** | Description of the resource. | [optional] 
**link** | **string** | Author URL to the resource. | [optional] 
**nickname** | **string** | The nickname for the resource. | [optional] 
**slug** | **string** | An alphanumeric identifier for the resource. | [optional] 
**registered_date** | **string** | Registration date for the resource. | [optional] 
**roles** | **string[]** | Roles assigned to the resource. | [optional] 
**password** | **string** | Password for the resource (never included). | [optional] 
**capabilities** | **string[]** | All capabilities assigned to the resource. | [optional] 
**extra_capabilities** | **string[]** | Any extra capabilities assigned to the resource. | [optional] 
**avatar_urls** | **string[]** | Avatar URLs for the resource. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


